-- Require awful for awful.widget and wibox for wibox.layout
local awful = require("awful")
local wibox = require("wibox")
local naughty = require("naughty")

batmon = wibox.layout.margin()
batmon.__index = batmon

batmon.widget = awful.widget.progressbar()
batmon.widget:set_border_color('#000000')
batmon.widget:set_ticks(true)
batmon.widget:set_ticks_gap(2)
batmon.widget:set_ticks_size(5)
batmon.widget:set_width(29)
batmon.widget:set_value(1)
batmon.widget:set_color('#FFFFFF')

batmon.timer = {}
batmon.tooltip = awful.tooltip({ objects = { batmon },})
batmon.warning_level = -math.huge
batmon.critical_level = -math.huge
batmon.warning_notified = false
batmon.critical_notified = false

batmon:set_widget(batmon.widget)
batmon:set_left(1)
batmon:set_right(4)
batmon:set_top(2)
batmon:set_bottom(2)

function batmon.batterymonitor(delay)
    local newbm = {}
    setmetatable(newbm, batmon)
    newbm:start(delay)
    return newbm
end

function batmon:start(t)
    self.timer = timer({ timeout = t })
    self.timer:connect_signal("timeout", function() self:update() end)
    self.timer:start()
    self:update()
end

function batmon:update() 
    local acpi_out = awful.util.pread('acpi')
    local battery_status = acpi_out:match('%a+, %d+%%')
    local battery_level = tonumber(battery_status:match('%d+'))
    local charging_status = battery_status:match('%a+')
    self.widget:set_value(battery_level / 100)

    local tooltipText = battery_level .. "% remaining"
    if charging_status == "Charging" then tooltipText = tooltipText .. " (charging)" end
    self.tooltip:set_text(tooltipText)

    if battery_level > 50 then
        self.widget:set_color('#00FF00')
    elseif battery_level > 25 then
        self.widget:set_color('#FFFF00')
    elseif battery_level > 10 then
        self.widget:set_color('#FF9900')
    else
        self.widget:set_color('#FF0000')
    end

    if battery_level <= self.critical_level and not self.critical_notified then
        self:show_critical()
    elseif battery_level <= self.warning_level and not self.warning_notified and not self.critical_notified then
        self:show_warning()
    end

    if battery_level > self.warning_level and self.warning_notified then
        self.warning_notified = false
    end

    if battery_level > self.critical_level and self.critical_notified then
        self.critical_notified = false
    end
end

function batmon:set_warning(percent)
    self.warning_level = percent
end

function batmon:set_critical(percent)
    self.critical_level = percent
end

function batmon:show_warning()
    naughty.notify({ preset = naughty.config.presets.normal,
                     title = "Your battery is running low (".. self.warning_level .."%)",
                     text = "You might want to plug in your PC." })
    self.warning_notified = true
end

function batmon:show_critical()
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Your battery is running critical (".. self.critical_level .. "%)",
                     text = "Plug your PC in to the nearest power source immediately!" })
    self.critical_notified = true
end
